<%--
  Created by IntelliJ IDEA.
  User: theo
  Date: 13/07/2017
  Time: 21:17
  To change this template use File | Settings | File Templates.
--%>

<%@page contentType="text/html;charset=UTF-8" language="java" %> <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%> <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>View a customer</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet"/> <link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet"/>
</head> <body>
<jsp:include page="/common/navbar.jsp"/>
<div class="page-header"> <h1>TPSIGL
    <small>View a customer</small> </h1>
</div>
<div id="wrapper" class="panel panel-default"> <div class="panel-body">
    <div class="row">
        <div class="col-md-3">First name</div>
        <div class="col-md-9">${customer.firstname}</div>
    </div>
    <div class="row">
        <div class="col-md-3">Last name</div>
        <div class="col-md-9">${customer.lastname}</div> </div>
    <div class="row">
        <div class="col-md-3">Email</div>
        <div class="col-md-9">${customer.email}</div>
    </div>
    <hr />
    <div>
        <a class="btn btn-default pull-left" href="<c:url
value="/customer/edit"/>">Edit customer</a>
        <a class="btn btn-danger pull-right" href="<c:url
value="/customer/delete"/>">Remove customer</a>
    </div>
    <div class="panel-footer">
</div>
</body>
</html>