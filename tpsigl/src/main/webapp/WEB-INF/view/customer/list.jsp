<%--
  Created by IntelliJ IDEA.
  User: theo
  Date: 13/07/2017
  Time: 19:48
  To change this template use File | Settings | File Templates.
--%>


<%@page contentType="text/html;charset=UTF-8" language="java" %> <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%> <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>List all customers</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet"/>
    <link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet"/>
</head>
<body>
<jsp:include page="/common/navbar.jsp"/>
<div class="page-header">
    <h1>TPSIGL <small>List all customers</small></h1>
</div>
<div id="wrapper" class="panel panel-default"> <div class="panel-body">
    <div class="list-group">
        <c:forEach var="customer" items="${listCustomers}">
            <a class="list-group-item" href="#">
                ${customer.firstname}
                ${customer.lastname}
            </a>
        </c:forEach>
        <a class="list-group-item" href="<c:url value="/customer/view/${customer.email}"/>">
    </div>
</div>
    </div>
</div>
</body>
</html>