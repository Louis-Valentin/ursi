<%--
  Created by IntelliJ IDEA.
  User: theo
  Date: 13/07/2017
  Time: 20:52
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-default">
    <div class="container-fluid"> <div class="navbar-header">
        <a class="navbar-brand" href="<c:url value="/"/>">TPSIGL /a>
    </div>
        <div class="collapse navbar-collapse"> <ul class="nav navbar-nav">
            <li><a href="<c:url value="/customer/add"/>">Add customer </a></li>
            <li><a href="<c:url value="/customer/list"/>">View customers </a></li> </ul>
            <form class="navbar-form navbar-right" role="search"> <div class="form-group">
                <input type="text" class="form-control" placeholder="Search"> </div>
                <button type="submit" class="btn btn-default">Submit</button> </form>
        </div> </div>
</nav>