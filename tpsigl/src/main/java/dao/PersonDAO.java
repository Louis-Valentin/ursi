package dao;

import model.Person;
import org.hibernate.Criteria;
import org.hibernate.Session;
import utils.HibernateUtils;

import java.util.List;

/**
 * Created by theo on 28/05/2017.
 */
public class PersonDAO {
    public PersonDAO(){}
    public List<Person> getPersons() {
        Session session = HibernateUtils.openSession();
        Criteria criteria = session.createCriteria(Person.class);

        List<Person> persons = (List<Person>) criteria.list();
        session.close();

        return persons;
    }

}
