package dao;

import model.Customer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import utils.HibernateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by theo on 13/07/2017.
 */
public class CustomerDAO {
    public List<Customer> listCustomers() {
        Session session = HibernateUtils.openSession();
        Criteria criteria = session.createCriteria(Customer.class);

        ArrayList<Customer> customers = (ArrayList<Customer>) criteria.list();

        session.close();
        return customers;
    }
    public void addCustomer(Customer c)
    {
        Session session = HibernateUtils.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(c);
        transaction.commit();
        session.close();
    }
    @SuppressWarnings("unchecked")
    public Customer getCustomerByEmail(String email) {
        Session session = HibernateUtils.openSession();
        Criteria criteria =
                session.createCriteria(Customer.class).add(Restrictions.eq("email", email)); List<Customer> customers = (List<Customer>) criteria.list();
        session.close();
        if (customers.size() != 1) return null;
        return customers.get(0); }

    public void updateCustomer(Customer c) {
        Session session = HibernateUtils.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(c);
        transaction.commit();
        session.close();
    }
    public void removeCustomer(String email) {
        Session session = HibernateUtils.getSessionFactory().openSession(); Transaction transaction = session.beginTransaction();
        Customer c = session.load(Customer.class, email); session.delete(c);
        transaction.commit();
        session.close();
    }
}
