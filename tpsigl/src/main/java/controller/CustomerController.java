package controller;

import model.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.CustomerService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by theo on 13/07/2017.
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {
    private CustomerService customerService;
    private Customer currentCustomer = null;

    public CustomerController(){
        customerService = new CustomerService();
    }

    public CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listCustomers(ModelMap model){
        List<Customer> customers = this.customerService.listCustomers();
        model.addAttribute("listCustomers", customers);
        return "customer/list";
    }


    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String addCustomer(ModelMap model) {
        model.addAttribute("customer", new Customer());
        return "customer/add";
    }


    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String saveCustomer(ModelMap model, @ModelAttribute("customer") Customer
            customer) {
        try {
        customerService.addCustomer(customer);
        model.addAttribute("listCustomers", this.customerService.listCustomers());
        return "customer/list";
    }
    catch (Exception e) {
        System.err.println(e);
        model.addAttribute("errorMessage", "Error: Cannot save customer");
        return "error";
    }
    }

    @RequestMapping(value = "view/{email:.+}", method = RequestMethod.GET)
    public String viewCustomer(ModelMap model, @PathVariable("email") String email) {
        currentCustomer = this.customerService.getCustomerByEmail(email);
        if (null == currentCustomer) {
            model.addAttribute("errorMessage", "Customer␣not␣found");
            return "error"; }
        model.addAttribute("customer", currentCustomer);
        return "customer/view"; }


    @RequestMapping(value = "edit", method = RequestMethod.GET) public String editCustomer(ModelMap model) {
        if (null == currentCustomer)
        {
            model.addAttribute("errorMessage", "Customer␣not␣found");
        return "error"; }

       model.addAttribute("customer", currentCustomer);
       return "customer/edit"; }
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String updateCustomer(ModelMap model, @ModelAttribute("customer") Customer
            customer) { try {
        customerService.updateCustomer(customer); currentCustomer = customer;
        return "customer/view";
    }
    catch (Exception e) {
        System.err.println(e);
        model.addAttribute("errorMessage", "Error: Cannot edit customer"); return "error";
    } }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public String deleteCustomer(ModelMap model) {
        if (null == currentCustomer)
        {
    model.addAttribute("errorMessage", "Customer␣not␣found");
return "error"; }
customerService.removeCustomer(currentCustomer.getEmail()); model.addAttribute("listCustomers", this.customerService.listCustomers());
return "customer/list";
        }
}


