package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by theo on 29/05/2017.
 */
@Controller
@RequestMapping("/hello")
public class HelloController {
    static final Logger LOG = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        LOG.trace("Hello World!");
        LOG.debug("How are you today?");
        LOG.info("I am fine.");
        LOG.warn("I love programming.");
        LOG.error("I am programming.");
        String test = "Created with Wildfly + Spring MVC + Hibernate + postgreSQL + maven + logback";
        model.addAttribute("msg", test );
        return "hello";
        }
}
