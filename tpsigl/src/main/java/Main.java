/**
 * Created by theo on 28/05/2017.
 */

import model.Car;
import model.Person;
import org.hibernate.Session;
import utils.HibernateUtils;

import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtils.openSession();

        Person stark_t = new Person("Tony", "Stark", new Date());
        Car jaguar = new Car("Jaguar", stark_t);
        Car twingo = new Car("Twingo", stark_t);


        session.save(stark_t);
        session.save(jaguar);
        session.save(twingo);

        List result = session.createCriteria(Person.class).list();

        for (Person p : (List<Person>) result) {
            System.out.println(String.format("Person: %s %s", p.getFirstName(),
                p.getLastName()));
            for (Car c : p.getCars())
                System.out.println(String.format("  - Car: %s", c.getModel()));
        }

        session.close();

        HibernateUtils.getSessionFactory();
    }
}
