package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by theo on 13/07/2017.
 */
@Entity
@Table(name = "customer")
public class Customer implements Serializable {
        @Id
        @Column
        private String email;

        @Column
        private String firstname;

        public void setEmail(String email) {
                this.email = email;
        }

        public void setFirstname(String firstname) {
                this.firstname = firstname;
        }

        public void setCommands(Set<Command> commands) {
                this.commands = commands;
        }

        public void setLastname(String lastname) {
                this.lastname = lastname;
        }

        @Column
        private String lastname;

        @OneToMany(mappedBy = "customer")
        private Set<Command> commands;

        public Customer(){
               // this.email = "toto";
               // this.firstname = "toto";
               // this.lastname = "toto";
        }

        public Customer(String email, String firstname, String lastname){
            this.email = email;
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public String getEmail() {
                return email;
        }

        public String getFirstname() {
                return firstname;
        }

        public String getLastname() {
                return lastname;
        }
}
