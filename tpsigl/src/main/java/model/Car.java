package model;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="car")
/**
 * Created by theo on 28/05/2017.
 */
public class Car {
    @Id
    @Column
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column
    private String model;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="owner_id", nullable=false)
    private Person owner;

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }



    public Car() {
        // used by Hibernate
    }

    public Car(String model, Person owner) {
        this.model = model;
        this.owner = owner;
    }
}
