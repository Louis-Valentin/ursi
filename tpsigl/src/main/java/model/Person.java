package model;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;



@Entity
@Table(name="person")
/**
 * Created by theo on 28/05/2017.
 */
public class Person {
    @Id
    @Column
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Column
    private String lastName;

    @Column
    private Date birthDate;

    public Set<Car> getCars() {
        return cars;
    }

    @OneToMany(fetch=FetchType.LAZY, mappedBy="owner")
    private Set<Car> cars;


    public Person() {
        // used by Hibernate

    }

    public Person(String firstName, String lastName, Date birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
}
