package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by theo on 13/07/2017.
 */
@Entity
@Table(name = "commmand")
public class Command implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column
        private Long id;

        @Column
        private Date deliveryDate;

        @OneToMany(mappedBy = "command")
        private Set<Product> products;

        @ManyToOne
        @JoinColumn(name = "customer")
        private Customer customer;

        public Command(){}
        public Command(Date deliveryDate, Set<Product> products, Customer customer){
            this.deliveryDate = deliveryDate;
            this.products = products;
            this.customer = customer;
        }
}
