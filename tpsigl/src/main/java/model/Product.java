package model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by theo on 13/07/2017.
 */
@Entity
@Table(name = "product")
public class Product implements Serializable{
    @Id
    @Column
    private String reference;

    @Column
    private String name;

    @Column
    private float price;

    @ManyToOne
    @JoinColumn(name = "command")
    private Command command;

    public Product(){}

    public Product(String reference, String name, float price){
        this.reference = reference;
        this.name = name;
        this.price = price;
    }
}
