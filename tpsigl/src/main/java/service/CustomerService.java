package service;

import dao.CustomerDAO;
import model.Customer;

import java.util.List;

/**
 * Created by theo on 13/07/2017.
 */
public class CustomerService {
    private CustomerDAO customerDAO;

    public CustomerService()
    {
        customerDAO = new CustomerDAO();
    }

    public List<Customer> listCustomers(){
        return this.customerDAO.listCustomers();
    }
    public void addCustomer(Customer c) throws Exception { this.customerDAO.addCustomer(c);
    }
    public Customer getCustomerByEmail(String email) { return this.customerDAO.getCustomerByEmail(email);
    }
    public void updateCustomer(Customer c) throws Exception { this.customerDAO.updateCustomer(c);
    }
    public void removeCustomer(String email) { this.customerDAO.removeCustomer(email);
    }
}
