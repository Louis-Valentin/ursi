<%--
  Created by IntelliJ IDEA.
  User: theo
  Date: 13/07/2017
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>

<%@page contentType="text/html;charset=UTF-8" language="java" %> <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%> <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Edit a customer</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet"/> <link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet"/>
</head> <body>
<jsp:include page="/common/navbar.jsp"/>
<div class="page-header"> <h1>TPSIGL
    <small>Edit a customer</small> </h1>
</div>
<div id="wrapper" class="panel panel-default"> <div class="panel-body">
<form:form method="post" action="update" commandName="customer"> <div class="form-group">
    <form:label path="firstname">First name</form:label>
    <form:input cssClass="form-control" path="firstname"/> </div>
    <div class="form-group">
    <form:label path="lastname">Last name</form:label> <form:input cssClass="form-control" path="lastname"/>
    </div>
    <div class="form-group">
    <form:label path="email">Email</form:label> <form:input cssClass="form-control" path="email"/>
    </div>
    <input type="submit" class="btn btn-default" value="<spring:message text="Update customer"/>"/>
</form:form> </div>
    <div class="panel-footer">
    </div> </div>
</body> </html>